# Repository for the VW-SozÖk-Seep R-Tutorial Winter 2018/19

## Dates & Location

Time: Always 18:00 to 20:00 (6pm to 8pm)

- Mon, Nov 19 2018
    - D5.1.003 Seminar room (30)
    
- Mon, Dec 3 2018
    - D5.1.003 Seminar room (30)
    
- Mon, Dec 10 2018
    - D5.1.003 Seminar room (30)
    
- Mon, Dec 17 2018
    - D4.0.144 Seminar room (30)

## Software downloads

***You don't have to install anything beforehands - We'll set up R at the beginning of the first session***

### R

- Debian/Ubuntu: `sudo apt install r-base`
    - also see `https://gitlab.com/r-students-WU/DotfilesAndOtherNonRcode/tree/master/ubuntu18_04`

- RHEL/CentOS/Fedora: `sudo yum install R`

- (open)SUSE: https://cran.r-project.org/bin/linux/suse/

- MacOS: https://cran.r-project.org/bin/macosx/

- Windows: https://cran.r-project.org/bin/windows/

### R-Studio

https://www.rstudio.com/products/rstudio/download/#download

### Git

https://git-scm.com/downloads

### Other Resources

https://imsmwu.github.io/MRDA2018/_book/index.html

