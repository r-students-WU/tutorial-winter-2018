// [[Rcpp::depends(RcppArmadillo)]]

#include <RcppArmadillo.h>

using namespace Rcpp;

// [[Rcpp::export]]
List iNG(const arma::mat & X, const arma::colvec & y,
              const arma::mat & V0inv, const arma::colvec & beta0, 
              const double nu0, const double s02, double h_init,
              size_t d_save, size_t d_burn, const arma::rowvec & x_pred) {
  
  const int N = X.n_rows, M = X.n_cols;
  arma::mat beta_store(d_save, M);
  arma::colvec h_store(d_save);
  arma::colvec y_pred(d_save);
  
  arma::mat XX = X.t() * X;
  arma::colvec Xy = X.t() * y;
  
  arma::mat V1inv(M, M);
  arma::mat V1(M, M);
  arma::colvec beta1(M);
  arma::colvec beta_draw(M);
  arma::colvec eps(N);
  
  double s12, h_draw = h_init;
  double nu1 = nu0 + N;
  
  size_t row, maxiter = d_save + d_burn;
  
  for(size_t iter = 0; iter <= maxiter; iter++){
    V1inv = V0inv + h_draw * XX;
    V1 = arma::inv(V1inv);
    beta1 = V1 * (V0inv * beta0 + h_draw * Xy);
    
    arma::colvec rnrm_temp(M, arma::fill::randn); 
    beta_draw = beta1 + arma::trans(arma::chol(V1)) * rnrm_temp;
    
    eps = y - X * beta_draw;
    s12 = (1/nu1) * (arma::as_scalar(eps.t() * eps) + nu0 * s02);
    h_draw = as<double>(Rcpp::rgamma(1, 0.5 * nu1, 1/(0.5 * nu1 * s12)));
    
    if (iter > d_burn){
      row = iter - d_burn - 1;
      beta_store.row(row) = beta_draw.t();
      h_store(row) = h_draw;
      y_pred(row) = arma::as_scalar(x_pred * beta_draw) + as<double>(Rcpp::rnorm(1, 0, sqrt(1/h_draw)));
    } 
  }
  return List::create(Named("Coefs") = beta_store,
                      Named("h") = h_store,
                      Named("Ypred") = y_pred);
}


