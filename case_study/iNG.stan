data {
  int<lower = 0> N; // nObs
  int<lower = 0> M; // nParams
  matrix[N, M] X; // independent vars
  vector[N]  y; // dependent var
  row_vector[M] x_pred; // x for prediction
  vector[M]  beta_0; // prior mean beta
  vector[M]  V_0; // prior covariance beta
  real  nu_0; // prior df h
  real  s_02; // inverse of prior mean h
}
transformed data{
  real alpha;
  real beta_h;

  alpha = 0.5 * nu_0;
  beta_h = alpha * s_02;
}
parameters{
  vector[M] beta;
  real<lower = 0> h;
}
model{
  beta ~ normal(beta_0, V_0);
  h ~ gamma(alpha, beta_h);
  y ~ normal(X * beta, 1/h);
}
generated quantities{
  real y_pred;
  y_pred = normal_rng(x_pred * beta, 1/h);
}
